#!/bin/sh
# sed -i -e 's/\r$//' arena-script-install-software.sh
# set permission: $ chmod a+rx file_name.sh
echo "***** Arena Shellscript Auto Install Software	*****"
hostnamectl
# uname -a
# Ex: Linux dev.linuxize.com 4.19.0-6-amd64 #1 SMP Debian 4.19.67-2+deb10u1 (2019-09-20) x86_64 GNU/Linux
# Linux - Kernel name. dev.linuxize.com - Hostname. 4.19.0-6-amd64 Kernel release. #1 SMP Debian 4.19.67-2+deb10u1 (2019-09-20) - Kernel version. x86_64 - Machine hardware name. GNU/Linux - Operating system name.

func_osChoice1()
{
  func_getInfoSoft_OS1
}

func_osChoice2()
{
  echo 'We now not support CentOS. Thank you.'
}

func_osChoice3()
{
  echo 'We now not support Debian. Thank you.'
}

func_osChoiceInvalid()
{
  echo 'Your choice is not available.'
}

func_getInfoSoft_OS1()
{
	echo "===== Checking Soft Info ====="
	echo "NodeJS: $(dpkg --list | grep nodejs)"
	echo "PM2: $(npm list -g pm2)"
	echo "GIT: $(git --version)"
	echo "PostgreSQL: $(dpkg --list | grep postgresql)"
	echo "Redis-Server: $(dpkg --list | grep redis-server)"
	echo "Nginx: $(dpkg --list | grep nginx)"
	echo "===== Checking Soft Info Done ====="
}

echo Your OS Server: 1 - Ubuntu, 2-CentOS, 3-Debian
read -p 'Linux OS: ' osChoice
if [ $osChoice -eq 1 ]
then
  func_osChoice1
elif [ $osChoice -eq 2 ]
then
  func_osChoice2
elif [ $osChoice -eq 3 ]
then
  func_osChoice3
else
  func_osChoiceInvalid
fi

func_installNodeJS_v12()
{
curl -fsSL https://deb.nodesource.com/setup_12.x | sudo -E bash -
apt-get install -y nodejs
echo "Installed NodeJS version $(node -v)"
}

func_installPM2()
{
npm install pm2 -g
echo "Installed PM2 version $(npm list -g pm2)"
}

func_installPostgreSQL()
{
apt-get update
apt-get --assume-yes install postgresql postgresql-contrib
echo "Installed PostgreSQL version $(psql -V)"
}

func_installRedis()
{
add-apt-repository ppa:redislabs/redis
apt-get update
apt-get --assume-yes install redis
echo "Installed Redis version $(redis-server --version)"
}

func_installNginx()
{
apt-get update
apt-get --assume-yes install nginx
echo "Installed Nginx version $(nginx -v)"
}

echo ""
echo "*** Install NodeJS? 1-Yes, 0-No ***"
read -p 'Your choice: ' nodejsInstall
if [ $nodejsInstall -eq 1 ]
then
  func_installNodeJS_v12
else
  echo "You choose not install NodeJS"
fi

echo ""
echo "*** Install PM2? 1-Yes, 0-No ***"
read -p 'Your choice: ' pm2Install
if [ $pm2Install -eq 1 ]
then
  func_installPM2
else
  echo "You choose not install PM2"
fi

echo ""
echo "*** Install PostgreSQL? 1-Yes, 0-No ***"
read -p 'Your choice: ' postgresInstall
if [ $postgresInstall -eq 1 ]
then
  func_installPostgreSQL
else
  echo "You choose not install PostgreSQL"
fi

echo ""
echo "*** Install Redis? 1-Yes, 0-No ***"
read -p 'Your choice: ' redisInstall
if [ $redisInstall -eq 1 ]
then
  func_installRedis
else
  echo "You choose not install Redis"
fi

echo ""
echo "*** Install NGINX? 1-Yes, 0-No ***"
read -p 'Your choice: ' nginxInstall
if [ $nginxInstall -eq 1 ]
then
  func_installNginx
else
  echo "You choose not install Nginx"
fi

func_getInfoSoft_OS1
